import QtQuick 2.12

import Ikinuki.Client 1.0

import "./BrowserView"

Row {
    required property Database db
    property int selectedView: 0
    property int selectedProvider: 0
    property bool browse: false
    Sidebar {
        id: sidebar
        maximized: selectedView == 0
        selectedElement: selectedProvider
        providers: db.Providers
    }
    ContentView {
        id: view
        viewSelected: selectedView == 1
        parentIndex: selectedProvider + (browse ? db.Providers.length : 0)
        providers: db.Providers
    }
    function mod(n, m) {
        return ((n % m) + m) % m;
    }
    Keys.onPressed: (event)=> {
        if (selectedView == 0) {
            if (event.key == Qt.Key_Right) {
                selectedView = 1;
            } else if (event.key == Qt.Key_Down) {
                selectedProvider = mod(selectedProvider + 1, db.Providers.length);
            } else if (event.key == Qt.Key_Up) {
                selectedProvider = mod(selectedProvider - 1, db.Providers.length);
            } else if (event.key == Qt.Key_Return) {
                browse = true;
                selectedView = 1;
            } else {
                return;
            }
            event.accepted = true;
        }
        else if (selectedView == 1) {
            view.Keys.pressed(event);
        }
    }
}
