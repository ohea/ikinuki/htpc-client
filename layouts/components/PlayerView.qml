import QtQuick 2.12

import Ikinuki.Client 1.0

Item {
    width: parent.width
    height: parent.height
    Mpv {
        id: mpvWidget
        anchors.fill: parent
    }
    Column {
        anchors.fill: parent
        anchors.leftMargin: parent.width * 0.05
        anchors.rightMargin: parent.width * 0.05
        Item {
            width: parent.width
            height: parent.height * 0.05
        }
        Text { // clock
            text: Qt.formatDateTime(new Date(), "h:mm:ss ap")
            font.pointSize: 20
            anchors.right: parent.right
            height: parent.height * 0.1
            color: "#99afb4"
        }
        Item {
            width: parent.width
            height: parent.height * 0.6
        }
        Text { // show title
            text: "title"
            font.pointSize: 40
            color: "#cdd7d9"
        }
        Row { // show details
            Text { // episode title
                text: "episodeTitle"
                font.pointSize: 20
                color: "#99afb4"
            }
            Text {
                text: " ● "
                font.pointSize: 20
                color: "#99afb4"
            }
            Text { // season number and episode number
                text: "S01 E01"
                font.pointSize: 20
                color: "#99afb4"
            }
            Text {
                text: " ● "
                font.pointSize: 20
                color: "#99afb4"
            }
            Text { // air date
                text: Qt.formatDateTime(new Date(), "MMM d, yyyy")
                font.pointSize: 20
                color: "#99afb4"
            }
            Text {
                text: " ● "
                font.pointSize: 20
                color: "#99afb4"
            }
            Text { // episode duration
                text: "duration"
                font.pointSize: 20
                color: "#99afb4"
            }
        }
        Item {
            width: parent.width
            height: parent.height * 0.01
        }
        Item { // playbar indicator bar
            width: parent.width
            height: parent.height * 0.01
            Rectangle {  // total duration
                color: "#99afb4"
                opacity: 0.5
                anchors.fill: parent
                x: parent.x + parent.width * 0.05
            }
            Rectangle { // completed duration
                color: "orange"
                anchors.left: parent.left
                width: parent.width * 0.4
                height: parent.height
                x: parent.x + parent.width * 0.05
            }
        }
        Item {
            width: parent.width
            height: parent.height * 0.01
        }
        Item {
            width: parent.width
            height: parent.height * 0.01
            Text { // time elapsed in episode
                text: "timeElapsed"
                font.pointSize: 20
                anchors.left: parent.left
                color: "#99afb4"
            }
            Text { // time remaninig in episode
                text: "timeRemaining"
                font.pointSize: 20
                anchors.right: parent.right
                color: "#99afb4"
            }
        }
    }
}
