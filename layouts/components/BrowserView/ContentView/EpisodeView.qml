import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.15

import Ikinuki.Client 1.0

import "./ShowView"

ScrollView {
    id: root
    property var episodeModel
    property int scrollIndex: 0
    contentHeight: root.height * 0.1 * episodeModel.length
    ScrollBar.vertical.position: scrollIndex / (episodeModel.length)
    clip: true
    Behavior on ScrollBar.vertical.position {
        NumberAnimation {
            duration: 200
            easing.type: Easing.Linear
        }
    }
    Column {
        Repeater {
            model: episodeModel
            Episode {
                episode: modelData
                selected: index == xIndex
                height: root.height * 0.1
                width: root.width
            }
        }
    }
}
