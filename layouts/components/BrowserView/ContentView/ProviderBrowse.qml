import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.15

import Ikinuki.Client 1.0

import "./ProviderBrowse"

Rectangle {
    id: root
    property var provider
    property bool viewExit: false
    property bool enterShow: false
    property var enterShowShow
    property int currentView: 0
    color: "#22282A"
    Row {
        anchors.fill: parent
        Item {
            height: parent.height
            width: parent.width * 0.025
        }
        Column {
            width: parent.width * 0.95
            height: parent.height
            Item {
                height: 40
                width: parent.width
            }
            CoverGrid {
                height: parent.height - 80
                width: parent.width
                id: coverGrid
                provider: root.provider
                shows: root.provider.showsAlphabetic
                selected: currentView == 0
            }
            Item {
                height: 40
                width: parent.width
            }
        }
        AlphabetSelector {
            id: alphabetSelector
            height: parent.height
            width: parent.width * 0.025
            selected: currentView == 1
        }
    }

    Keys.onPressed: (event)=> {
        if (currentView == 0) { // grid
            if (event.key == Qt.Key_Left) {
                if (coverGrid.xIndex == 0) {
                    viewExit = true;
                } else {
                    coverGrid.xIndex--;
                }
            } else if (event.key == Qt.Key_Right) {
                if (coverGrid.xIndex == coverGrid.numColumns - 1) {
                    currentView = 1;
                } else {
                    coverGrid.xIndex++;
                }
            } else if (event.key == Qt.Key_Down) {
                var max = Math.floor(coverGrid.shows.length / coverGrid.numColumns);
                if (coverGrid.yIndex < max) {
                    coverGrid.yIndex++;
                    if (coverGrid.scrollIndex < coverGrid.yIndex - 1.5) {
                        coverGrid.scrollIndex++;
                    }
                }
            } else if (event.key == Qt.Key_Up) {
                if (coverGrid.yIndex > 0) {
                    coverGrid.yIndex--;
                    if (coverGrid.scrollIndex > coverGrid.yIndex + 0.5) {
                        coverGrid.scrollIndex--;
                    }
                }
            } else if (event.key == Qt.Key_Return) {
                enterShowShow = root.provider.getShow(root.provider.showsAlphabetic[(coverGrid.xIndex + coverGrid.yIndex * coverGrid.numColumns)])
                enterShow = true;
            }
        } else if (currentView == 1) { // alphabet
            if (event.key == Qt.Key_Left) {
                currentView = 0;
            } else if (event.key == Qt.Key_Up) {
                if (alphabetSelector.yIndex > 0) {
                    alphabetSelector.yIndex--;
                }
            } else if (event.key == Qt.Key_Down) {
                if (alphabetSelector.yIndex < alphabetSelector.letters.length - 1)
                    alphabetSelector.yIndex++;
            }
        } else {
            return
        }
        event.accepted = true;
    }
}
