import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.15

import Ikinuki.Client 1.0

import "./ProviderHome"

Rectangle {
    id: root
    property var provider
    property int ySelect: 0
    property bool viewExit: false
    property bool enterShow: false
    property var enterShowShow
    color: "#22282A"
    Row {
        Item {
            height: 10
            width: 40
        }
        Column {
            height: tabView.height
            width: tabView.width - 40
            ShowDetails {
                z: 1.2
                height: parent.height * 0.25
                width: parent.width
                show: provider.getShow(elementColumn.children[ySelect].showId)
            }
            ScrollView {
                z: 1.0
                height: parent.height * 0.75
                width: parent.width
                contentWidth: 2000
                contentHeight: 2000
                ScrollBar.vertical.position: ySelect * 0.22
                Behavior on ScrollBar.vertical.position {
                    NumberAnimation {
                        duration: 200
                        easing.type: Easing.Linear
                    }
                }
                Flickable{
                    clip: true
                    Column {
                        id: elementColumn
                        spacing: 20
                        width: parent.width
                        Element {
                            title: "In Progress"
                            provider: root.provider
                            repeaterModel: provider.inProgress
                            elemSelected: viewSelected && (0 == ySelect)
                        }
                        Element {
                            title: "Recently Added"
                            provider: root.provider
                            repeaterModel: provider.recentlyAdded
                            elemSelected: viewSelected && (1 == ySelect)
                        }
                    }
                }
            }
        }
    }

    Keys.onPressed: (event)=> {
        if (event.key == Qt.Key_Left) {
            if (elementColumn.children[ySelect].xIndex == 0) {
                ySelect = 0;
                viewExit = true;
                for (var i = 0; i < elementColumn.children.length; i++) {
                    elementColumn.children[i].xIndex = 0;
                }
            } else {
                elementColumn.children[ySelect].xIndex--;
            }
        } else if (event.key == Qt.Key_Right) {
            elementColumn.children[ySelect].xIndex++;
        } else if (event.key == Qt.Key_Down) {
            ySelect++;
        } else if (event.key == Qt.Key_Up) {
            ySelect--;
        } else if (event.key == Qt.Key_Return) {
            enterShowShow = provider.getShow(elementColumn.children[ySelect].showId)
            enterShow = true;
        }
        event.accepted = true;
    }

}
