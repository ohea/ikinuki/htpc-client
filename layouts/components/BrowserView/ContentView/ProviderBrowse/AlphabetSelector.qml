import QtQuick 2.12
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.12

Column {
    property int yIndex: 0
    property bool selected
    property var letters: ['#', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    Item {
        height: 20
        width: parent.width
    }
    ScrollView {
        height: parent.height - 30
        width: parent.width
        ScrollBar.vertical.position: {
            if (yIndex > 20) {
                return (5 - (25 - yIndex)) * (0.048);
            } else {
                return 0;
            }
        }
        Flickable {
            clip: true
            Column {
                spacing: 10
                Repeater {
                    model: letters
                    Item {
                        width: 40
                        height: 40
                        anchors.horizontalCenter: parent.horizontalCenter
                        property bool s: selected && (index == yIndex)
                        DropShadow {
                            anchors.fill: selector
                            verticalOffset: 5
                            samples: 20
                            color: "black"
                            opacity: 0.5
                            source: selector
                            visible: s
                        }
                        Rectangle {
                            id: selector
                            anchors.fill: parent
                            color: "white"
                            visible: s
                            radius: 10
                        }
                        Text {
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.verticalCenterOffset: 4
                            text: modelData
                            color: s ? "black" : "gray"
                            font.pointSize: 18
                        }
                    }
                }
            }
        }
    }
    Item {
        height: 20
        width: parent.width
    }
}
