import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.15

import Ikinuki.Client 1.0

ScrollView {
    id: root
    property bool selected
    property int yIndex: 0
    property int xIndex: 0
    property int scrollIndex: 0
    property int numColumns: 6
    property var provider
    property var shows
    contentHeight: Math.floor(shows.length / numColumns) * 400
    ScrollBar.vertical.position: scrollIndex / Math.floor(shows.length / numColumns)
    Behavior on ScrollBar.vertical.position {
        NumberAnimation {
            duration: 200
            easing.type: Easing.Linear
        }
    }
    Grid {
        columns: numColumns
        topPadding: 20
        Repeater {
            id: repeater
            model: shows
            BrowseShow {
                show: provider.getShow(modelData)
                elemSelected: selected && (index == (yIndex * numColumns + xIndex))
            }
        }
    }
}
