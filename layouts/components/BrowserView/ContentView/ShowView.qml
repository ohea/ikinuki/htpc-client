import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.15

import Ikinuki.Client 1.0

import "./ShowView"

Rectangle {
    property var show
    property bool viewExit: false
    property bool enterShow: false
    property int xIndex: 0
    color: "#22282A"

    Column {
        anchors.fill: parent
        anchors.leftMargin: parent.width * 0.01
        anchors.rightMargin: parent.width * 0.01
        anchors.topMargin: parent.height * 0.05
        anchors.bottomMargin: parent.height * 0.01
        Text { // header
            height: parent.height * 0.1
            width: parent.width
            text: show.title
            font.pointSize: 20
            color: "#cdd7d9"
        }
        Row { // main view
            height: parent.height * 0.8
            width: parent.width
            spacing: parent.width * 0.01
            Item {
                height: parent.height
                width: height * 0.68
                Image {
                    source: show.poster
                    anchors.fill: parent
                    mipmap: true
                }
            }
            EpisodeView {
                id: episodeView
                height: parent.height
                width: parent.width * 0.65
                episodeModel: show.episodes
            }
        }
        Item {
            height: parent.height * 0.1
            width: parent.width
        }
    }

    Keys.onPressed: (event)=> {
        if (event.key == Qt.Key_Up) {
            if (xIndex > 0) {
                xIndex--;
                if (episodeView.scrollIndex > xIndex) {
                    episodeView.scrollIndex--;
                }
            }
        } else if (event.key == Qt.Key_Down) {
            if (xIndex < show.episodes.length - 1) {
                if (episodeView.scrollIndex < (xIndex - 8)) {
                    episodeView.scrollIndex++;
                }
                xIndex++;
            }
        } else if (event.key == Qt.Key_Escape) {
            viewExit = true;
        }
        event.accepted = true;
    }
}
