import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.15

import Ikinuki.Client 1.0

Item {
    property var episode
    property bool selected

    Rectangle {
        anchors.fill: parent
        color: "white"
        visible: selected
        radius: 10
    }
    Row {
        anchors.fill: parent
        Item {
            height: parent.height
            width: parent.width * 0.04
        }
        Column {
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 4
            Text {
                text: {
                    if (modelData.season == 0) {
                        return "S" + modelData.episode + " " + modelData.title
                    } else {
                        return String(modelData.episode).padStart(2, "0") + ". " + modelData.title
                    }
                }
                font.pointSize: 20
                color: selected ? "#3c3c3c" : "#99afb4"
            }
            Item {
                height: parent.height * 0.1
                width: parent.width
            }
            Text {
                text: " air date"
                font.pointSize: 12
                color: selected ? "#3c3c3c" : "#99afb4"
            }
        }
    }
}
