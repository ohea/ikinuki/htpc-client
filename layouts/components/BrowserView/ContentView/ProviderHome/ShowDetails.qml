import QtQuick 2.12

Row {
    property var show
    Item {
        height: parent.height
        width: 16
    }
    Column {
        height: parent.height
        width: parent.width - 16
        spacing: 20
        Item {
            height: 20
            width: parent.width
        }
        Text {
            text: show.title
            font.pointSize: 30
            color: "#cdd7d9"
        }
        Text {
            text: show.year
            font.pointSize: 15
            color: "#99afb4"
        }
        Text {
            text: show.description
            font.pointSize: 13
            color: "#cdd7d9"
            width: parent.width * 0.6
            wrapMode: Text.WordWrap
            maximumLineCount: 4
            elide: Text.ElideRight
        }
    }
}
