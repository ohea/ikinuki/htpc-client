import QtQuick 2.12
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.12

Item {
    property var show
    property int baseHeight: 350
    property int childHeight: baseHeight
    property int childWidth: baseHeight * 0.68
    property int childHeightExpanded: baseHeight * 15 / 14
    property int childWidthExpanded: (baseHeight * 15 / 14) * 0.68
    property int borderWidth: 3
    property int borderRadius: 1
    property int borderRectHeight: childHeight + (borderWidth * 2)
    property int borderRectWidth: childWidth + (borderWidth * 2)
    property int borderRectHeightExpanded: childHeightExpanded + (borderWidth * 2)
    property int borderRectWidthExpanded: childWidthExpanded + (borderWidth * 2)

    height: baseHeight
    width: (baseHeight * 8 / 7) * 0.68
    Item {
        id: parentElem
        state: (elemSelected && (index == xIndex)) ? "selected" : "deselected"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.fill: parent
        DropShadow {
            id: dropShadow
            anchors.fill: img
            verticalOffset: 15
            samples: 80
            opacity: 0.5
            color: "black"
            source: img
        }
        Rectangle {
            id: rect
            color: "transparent"
            border.color: "orange"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            radius: borderRadius
        }
        Image {
            id: img
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: show.poster
            mipmap: true
        }
        states: [
            State {
                name: "deselected"
                PropertyChanges {
                    target: rect
                    border.width: 0
                    height: borderRectHeight
                    width: borderRectWidth
                }
                PropertyChanges {
                    target: dropShadow
                    visible: false
                }
                PropertyChanges {
                    target: img
                    width: childWidth
                    height: childHeight
                }
            },
            State {
                name: "selected"
                PropertyChanges {
                    target: rect
                    border.width: borderWidth
                    height: borderRectHeightExpanded
                    width: borderRectWidthExpanded
                }
                PropertyChanges {
                    target: dropShadow
                    visible: true
                }
                PropertyChanges {
                    target: img
                    width: childWidthExpanded
                    height: childHeightExpanded
                }
            }
        ]
        transitions: [
            Transition {
                NumberAnimation {
                    properties: "border.width,width,height"
                    duration: 100
                    easing.type: Easing.Linear
                }
            }
        ]
    }
}
