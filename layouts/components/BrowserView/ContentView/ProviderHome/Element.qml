import QtQuick 2.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.15

Column {
    id: root
    property var title
    property var provider
    property var repeaterModel
    property bool elemSelected
    property int xIndex: 0
    property int showId: repeaterModel[xIndex]
    width: parent.width
    Item {
        width: 10
        height: 20
    }
    Row {
        Item {
            height: parent.height
            width: 16
        }
        Text {
            text: title
            font.pointSize: 15
            color: "#cdd7d9"
        }
    }
    Item {
        width: 10
        height: 15
    }
    ScrollView {
        width: parent.width
        height: 400
        ScrollBar.horizontal.position: xIndex * 0.135
        Behavior on ScrollBar.horizontal.position {
            NumberAnimation {
                duration: 200
                easing.type: Easing.Linear
            }
        }
        Flickable {
            clip: true
            Column {
                Item {
                    width: parent.width
                    height: 20
                }
                Row {
                    Repeater {
                        id: repeater
                        property var provider: modelData
                        model: repeaterModel
                        Show {
                            show: provider.getShow(modelData)
                        }
                    }
                }
            }
        }
    }
 }
