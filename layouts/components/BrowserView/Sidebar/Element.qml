import QtQuick 2.12
import QtGraphicalEffects 1.12

Item {
    property var provider
    property bool selected: false
    property bool maximized

    width: parent.width * 0.9
    height: parent.height * 0.07
    anchors.right: parent.right
    DropShadow {
        anchors.fill: selector
        verticalOffset: 5
        samples: 20
        color: "black"
        opacity: 0.5
        source: selector
        visible: selected && maximized
    }
    Rectangle {
        id: selector
        color: "white"
        height: parent.height
        width: parent.width * 0.95
        visible: selected && maximized
        radius: 10
    }
    Row {
        anchors.fill: parent
        Item {
            height: parent.height
            width: parent.width * 0.09
        }
        Item {
            anchors.verticalCenter: parent.verticalCenter
            height: 40
            width: 40
            Image {
                anchors.fill: parent
                id: logo
                source: provider.logo
            }
            ColorOverlay {
                anchors.fill: logo
                source: logo
                color: selected ? "#3c3c3c" : "#99afb4"
            }
        }
        Item {
            height: parent.height
            width: parent.width * 0.07
        }
        Text {
            id: text
            font.pointSize: 12
            text: provider.name
            color: selected ? "#3c3c3c" : "#99afb4"
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 4
            visible: maximized
        }
    }
}
