import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.15

import Ikinuki.Client 1.0

import "./ContentView"

StackLayout {
    id: tabView
    property var providers: []
    property bool viewSelected
    property int parentIndex

    property int ySelect: 0
    property bool showViewActive: false

    currentIndex: showViewActive ? tabView.children.length - 3 : parentIndex

    state: viewSelected ? "selected" : "deselected"
    width: parent.width * viewSelected ? 0.95 : 0.8
    height: parent.height

    Repeater {
        model: providers
        ProviderHome {
            provider: modelData
        }
    }
    Repeater {
        model: providers
        ProviderBrowse {
            provider: modelData
        }
    }
    ShowView {
        id: showView
        show: providers[0].getShow(providers[0].showsAlphabetic[0])
    }
    states: [
        State {
            name: "deselected"
            PropertyChanges {
                target: tabView
                width: parent.width * 0.8
            }
        },
        State {
            name: "selected"
            PropertyChanges {
                target: tabView
                width: parent.width * 0.95
            }
        }
    ]
    transitions: [
        Transition {
            NumberAnimation {
                properties: "width"
                duration: 150
                easing.type: Easing.Linear
            }
        }
    ]

    function getCurrentIndex() {
        if (showViewActive) {
            var x = tabView.children.length - 1;
        } else {
            var x = currentIndex > (providers.length - 1) ? currentIndex + 1 : currentIndex;
        }
        return x;
    }

    Keys.onPressed: (event)=> {
        var x = getCurrentIndex()
        tabView.children[x].Keys.pressed(event);
        if (tabView.children[x].enterShow) {
            showViewActive = true;
            showView.show = tabView.children[x].enterShowShow;
        } else if (tabView.children[x].viewExit) {
            if (showViewActive) {
                showViewActive = false;
                tabView.children[x].xIndex = 0;
                tabView.children[x].viewExit = false;
                tabView.children[getCurrentIndex()].enterShow = false;
            } else {
                tabView.children[x].viewExit = false;
                parent.browse = false;
                parent.selectedView = 0;
            }
        }
        event.accepted = true;
    }
}
