import QtQuick 2.12

import Ikinuki.Client 1.0

import "./Sidebar"

Rectangle {
    id: rect
    property var providers
    property bool maximized
    property int selectedElement
    width: parent.width * 0.2
    height: parent.height
    color: "#22282A"
    state: maximized ? "maximized" : "minimized"
    Column {
        anchors.fill: parent
        Header {}
        Repeater {
            id: sidebarElements
            model: providers
            Element {
                provider: modelData
                maximized: rect.maximized
                selected: (index == selectedElement) ? true : false
            }
        }
    }
    states: [
        State {
            name: "minimized"
            PropertyChanges {
                target: rect
                width: parent.width * 0.05
            }
        },
        State {
            name: "maximized"
            PropertyChanges {
                target: rect
                width: parent.width * 0.2
            }
        }
    ]
    transitions: [
        Transition {
            NumberAnimation {
                properties: "width"
                duration: 150
                easing.type: Easing.Linear
            }
        }
    ]
}
