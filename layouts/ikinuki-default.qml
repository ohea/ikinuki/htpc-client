import QtQuick 2.12
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Imagine 2.12
import QtQuick.Window 2.0

import Ikinuki.Client 1.0

import "./components"

ApplicationWindow {
    width: 1920
    height: 1080
    id: window
    visible: true
    Database {
        id: database
    }
    StackLayout {
        id: mainTabView
        anchors.fill: parent
        currentIndex: 0
        BrowserView {
            id: browserView
            focus: true
            db: database
        }
        PlayerView {
            id: playerView
        }
        Keys.onPressed: (event)=> {
            if (event.key == Qt.Key_L) {
                currentIndex = 1
                playerView.children[0].play("./test1.mkv")
            }
        }
    }
}
